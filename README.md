Recurly Test
===========

Summary
-------------
This is sample for how to build test automation from 0 to CI. Here is the list what we need:
* Java (Most  of code are in Java)
* Selenium (Most popular tool for UI automation testing, many tools are built based on it)
* Rest-Assured (API testing framework)
* jUnit (Test framework to handle and organize your test case / suite / categories and many more annotations)
* Gradle (Great build tool have advantage of both ant and maven)
* Jenkins (CI server, handle jobs with many popular build tools and version control tools)
* Git (Use git family to manage your source code)
* Docker (Make your environment much easier)
* Groovy (The gradle configure file is in groovy)
* This sample is based on recurly app.

> It is a standard gradle java-library project structure. Since we are building a test project, most of the code are in src/test/java folder.

> Run commnand "gradle build" will execute test case and generate well formed html report in build/report/test folder.

>Note: Ideally the best practice is implementing testing automation in 3 layers. 
1. Core layer is for basic function to handle automation testing but nothing to do with specific product. 
2. The second layer is the library based on product, handle all interaction with product.
3. 3rd layer is test scripts we create only contain test steps and verifications.

> But this project is only a demo. only build it from 2nd layer, just ignored the core layer based on existing tools to handle framework function, such as, support different browser, handle data driver, handle logging better, or generating better custom test report. We only focus on to create the POM and COM for product itself and give a simple sample to show how to use them.

## Structures
1. All testing code are in src/test/java
2. All code are under com.recurly.yaam package
3. Package com.recurly.yaam is for test cases
4. Package com.recurly.yaam.lib is for lib file which can use by others
5. Package com.recurly.yaam.pages is the folder for page object models
6. Package com.recurly.yaam.componets is folder for component object models
7. Package com.recurly.yaam.PageFacotory is for the sample for POM using PageFactory.
8. Package com.recurly.yaam.api is the folder for api automation testing.

> If you can't run this locally on your machine, feel free to send me email at yunpeng-li@hotmail.com I can help you to run it or put the test result on the gitlab. If we build folder there, the test result should in folder build/report. 

> If you have any question or need more information, feel free to send me email.

## Branches
Master contains all code for all branches. This is a gradle project. Easy auto build and continue integrate.

### 1-only-selenium
Create java class in main folder with main function. We can just run it. we can see 
1. Login with valid user correctly.
2. Click account link in left-side panel in home page.
3. Check the account number is correct. 

### 2-junit-selenium
Add jUnit as test framework to handle many staffs, so make create test scripts much easier.
1. @Before can handle setup and login.
2. @After can do some cleanup.
3. @Test mark multiple function as test case.
4. Run it as jUnit test.
5. Report generated automatically.
6. Also can check be better report if run from Gradle.

### 3-page-object
Abstract Page object model, easy to understand and create test case.
1. Build login page model
2. Build home page model
3. Build account page model
4. Improve the script with model

### 4-component-object
1. Add SideBar class to support navigation
2. Add PageBanner class
3. Add PageHead class

This 3 parts covered all common stuffs in pages

### 5-page-factory
More readable way to implement POM and COM. 

This part is only the simple samples. Need more work to do to get it work.

### 6-yaam-class
Add Yaam class as test parent class to handle common stuffs. 

### 7-api-test
Add api test support based rest assured and a simple api test sample.


## TODO
Still have a lot of things to do to improve the frame work to make automation easier. 
1. Improve testing log with log4j and java log on different information level.
2. Build business object model to handle more action related to business logic.
3. Parameterize the variables in test scripts.
4. Data driven
5. Build common library to handle task in need to do in many scripts, such as clean up date after test finished.
6. Better log, maybe configure in different level. 
7. Support test suites
8. Support categories.
9. Exception handle library.
10. Handle configures 
11. Handle different browsers
12. More framework features 
