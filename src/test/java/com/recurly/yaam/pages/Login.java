package com.recurly.yaam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {

	WebDriver driver;
	
	By email = By.id("user_email");
	By password = By.id("user_password");
	By loginButton = By.id("submit_button");
	By titleText = By.xpath("//*[@id=\"logo\"]/span");
	
	public Login(WebDriver driver){
		this.driver = driver;
	}
	
	//Set email in text box
	public void setEmail(String strEmail) {
		driver.findElement(email).clear();
		driver.findElement(email).sendKeys(strEmail);
	}
	
	//Set password in password text box
	public void setPassword(String strPassword) {
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(strPassword);
	}
	
	//Click on login button
	public void clickLogin() {
		driver.findElement(loginButton).submit();
	}
	
	//Get the recurly page title (the logo text, not the page title).
	public String getLoginTitle(){
		return driver.findElement(titleText).getText();
	}
	
	//Log user to recurly app with email and password.
	public void loginRecurly(String strEmail, String strpPassword) {
		System.out.println("Start login recurly");
		System.out.println("Go to https://app.recurly.com");
		driver.get("https://app.recurly.com");
		this.setPassword(strpPassword);
		this.setEmail(strEmail);
		this.clickLogin();
		
		System.out.println("End login recurly");
	}

}
