package com.recurly.yaam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Home {
	WebDriver driver;
	By enableProductionMode = By.cssSelector("a[href='/configuration/production_mode']");
	
	public Home(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickEnableProductionMode(){
		driver.findElement(enableProductionMode).click();
	}
	
	public String getEnableProductionModeButtonText(){
		return driver.findElement(enableProductionMode).getText();
	}
}
