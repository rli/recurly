package com.recurly.yaam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountList {
	WebDriver driver;
	By newAccountButton = By.cssSelector("a.Button.Button--primary");
	By accountNumberAtMainBottom = By.xpath("/html/body/div[1]/div[2]/div/div/div[3]/div[2]/div/div[2]/div[1]/b");
	
	public AccountList(WebDriver driver){
		this.driver = driver;
	}
	
	public String getButtonText(){
		return driver.findElement(newAccountButton).getText();
	}
	
	public void clickNewAccountButton(){
		driver.findElement(newAccountButton).click();
	}
	
	public String getAccountNumberAtMainBottomText(){
		return driver.findElement(accountNumberAtMainBottom).getText();
	}

}
