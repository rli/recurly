package com.recurly.yaam.api;

import io.restassured.RestAssured;

import org.junit.Before;


public class YaamApi {
	
	@Before
	public void setUp(){
		RestAssured.baseURI = "https://yaam.recurly.com";
		RestAssured.port = 443;
	}

}
