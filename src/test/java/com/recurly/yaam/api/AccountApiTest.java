package com.recurly.yaam.api;

import org.junit.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class AccountApiTest extends YaamApi {
	
	@Test
	public void getAccountRecord(){
		given().
			auth().preemptive().basic("423a3cd81a9e4c80960f030a953f2b66","").
		when().
			get("/v2/accounts/1").
		then().
			statusCode(200).
			body("account.account_code", equalTo("1"),
					"account.first_name", equalTo("Verena"),
					"last_name", equalTo("Example"));
	}
	
	@Test
	public void deleteAccountRecord(){
		given().
			auth().preemptive().basic("423a3cd81a9e4c80960f030a953f2b66", "").
		when().
			delete("/v2/accounts/1").
		then().
			statusCode(204);
	}
	
	@Test
	public void createAccuntRecord(){
		String request = "<account>" +
				"<account_code>7</account_code>" +
  				"<first_name>Xiu</first_name>" +
				"</account>";
		given().
			auth().preemptive().basic("423a3cd81a9e4c80960f030a953f2b66", "").
			body(request).
		when().
			post("/v2/accounts").
		then().
			statusCode(201).
			body("account.account_code", equalTo("7"),
					"account.first_name", equalTo("Xiu"));

	}

}
