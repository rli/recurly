package com.recurly.yaam.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SideBar {
	WebDriver driver;
	
	@FindBy()
	WebElement recurlyLogo;
	
	@FindBy()
	WebElement accountsLink;
	
	
	public SideBar(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickRecurlyLogo(){
		recurlyLogo.click();
	}
	
	public void clickAccountsLink(){
		accountsLink.click();
	}
}
