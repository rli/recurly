package com.recurly.yaam.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageHead {
	WebDriver driver;
	
	@FindBy()
	WebElement pageHeadTitle;
	
	public PageHead(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
