package com.recurly.yaam.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {
	WebDriver driver;
	
	@FindBy()
	WebElement email;
	
	@FindBy()
	WebElement password;
	
	@FindBy()
	WebElement loginButton;
	
	public Login(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setEmail(String strEmail){
		email.sendKeys(strEmail);
	}

}
