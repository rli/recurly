package com.recurly.yaam.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageBanner {
	WebDriver driver;
	
	@FindBy()
	WebElement breadCrumb;
	
	public PageBanner(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
