package com.recurly.yaam.lib;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.recurly.yaam.pages.Login;

public class Yaam {
	protected static WebDriver driver;
	static Login objLogin;
	
	@BeforeClass
	public static void setUp() {
		driver = new ChromeDriver();
		objLogin = new Login(driver);
		objLogin.loginRecurly("yunpeng-li@hotmail.com", "welcome2Yaam");
	}
	
	@AfterClass
	public static void tearDown(){
		driver.quit();
	}

}
