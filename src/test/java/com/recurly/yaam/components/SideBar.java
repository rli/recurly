package com.recurly.yaam.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SideBar {
	WebDriver driver;
	By sbLogo = By.cssSelector("a.Sidebar-logo");
	By siteSelector = By.cssSelector("div.SiteSelector-site.SiteSelector-site--current");
	By setupProgressLink;
	By link1;
	By link2;
	By link3;
	By link4;
	By ustomersLink;
	By accountsLink = By.linkText("Accounts");
	By subscriptionsLink;
	By transactionsLink;
	
	By analyticsLink;
	By dashboardLink;
	By subscribersLink;
	By plansLink;
	By monthlyRecurringRevenueLink;
	By billingsLink;
	By churnAnalysisNewLink;
	By planPerformanceNewLink;
	By recoveredRevNewLink;
	By exportsLink;
	
	By configurationLink;
	By siteSettingsLink;
	By cPlansLink;
	By measuredUnitsLink;
	By giftCardSettingsLink;
	By invoiceSettingsLink;
	By couponsLink;
	By currenciesLink;
	By taxesLink;
	By emailTemplateLink;
	By paymentGatewaysLink;
	By hostedPageSettingsLink;
	By fraudManagementLink;
	By duringManagementLink;
	By analyticsSettingsLink;
	
	By integrationsLink;
	By mailChimpLink;
	By salesforceLink;
	By quickBooksOnlineLink;
	
	By developersLink;
	By apiCredentialsLink;
	By webhooksLink;
	
	By adminLink;
	
	By usersLink;
	By choosePlanLink;
	
	
	public SideBar(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickAccountsLink(){
		driver.findElement(accountsLink).click();
	}
	
	public void clickSidebarLogo(){
		driver.findElement(sbLogo).click();
	}
}
