package com.recurly.yaam.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageHead {
	WebDriver driver;
	By headTitile = By.cssSelector("div.Page-title");
	By headTooltip = By.cssSelector("div.Head-tooltip");
	By pageAction = By.cssSelector("div.Page-action");
	
	public PageHead(WebDriver driver){
		this.driver = driver;
	}
	
	public String getPageHeadTitle(){
		return driver.findElement(headTitile).getText();
	}
}
