package com.recurly.yaam.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageBanner {
	WebDriver driver;
	By breadCrumb = By.cssSelector("div.BreadCrum");
	By userProfileLink = By.cssSelector("a[href='https://yaam.recurly.com/profile'");
	By logoutLink = By.linkText("Log Out");
	
	public PageBanner(WebDriver driver){
		this.driver = driver;
	}
	
	public String getHomePageProfileUserName(){
		return driver.findElement(userProfileLink).getText();
	}
	
	public void clickLogoutLink(){
		driver.findElement(logoutLink).click();
	}
}
