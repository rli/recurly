package com.recurly.yaam;

import com.recurly.yaam.components.SideBar;
import com.recurly.yaam.lib.Yaam;
import com.recurly.yaam.pages.AccountList;
import com.recurly.yaam.pages.Home;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AccountPageTest extends Yaam {

	AccountList objAccountList;
	Home objHome;
	SideBar objSb;
		
	@Test
	public void verifyAccountLink() {
		System.out.println("Start verify account nuber");
		objHome = new Home(driver);
		objSb = new SideBar(driver);
		objSb.clickAccountsLink();
		objAccountList = new AccountList(driver);
		String accountNumber = objAccountList.getAccountNumberAtMainBottomText();		
		assertEquals("failure - account number is wrong", "all 1", accountNumber);
		System.out.println("End verify account nuber");
	}
	
	@Test
	public void verifyNewAccountButton() {
		objSb = new SideBar(driver);
		objSb.clickAccountsLink();
		objAccountList = new AccountList(driver);
		objAccountList.clickNewAccountButton();
	}
}
