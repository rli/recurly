package com.recurly.yaam;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class AccountPageTest {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.get("https://app.recurly.com");
		
		System.out.println("Go to https://app.recurly.com");
		assertEquals("failure - login page title is not correct", "Log In to Recurly", driver.getTitle());
		
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement email = driver.findElement(By.id("user_email"));
		email.sendKeys("yunpeng-li@hotmail.com");
		WebElement password  = driver.findElement(By.id("user_password"));
		password.sendKeys("welcome2Yaam");
		
		WebElement logInButton = driver.findElement(By.id("submit_button"));
		logInButton.submit();
		
		System.out.println("Assert page title is 'Getting Started With Recurly — Recurly'");
		assertEquals("failure - home page brwoser title is not correct", "Getting Started With Recurly — Recurly", driver.getTitle());		
		
		WebElement pageTitle = driver.findElement(By.className("Page-title"));
		assertEquals("failure - home Page title is not correct", "Getting Started With Recurly", pageTitle.getText());
		
		driver.findElement(By.linkText("Accounts")).click();
		WebElement accountNumber = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/div[3]/div[2]/div/div[2]/div[1]/b"));
		assertEquals("failure - account number is wrong", "all 0", accountNumber.getText());
		
		driver.quit();	
	}

}
